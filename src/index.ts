import { Request, ResponseToolkit } from '@hapi/hapi';
import { readImages, Site } from 'cms-monkey-lib';
import { BuildPage, PreBuildPage } from './utils';
import { THapiCustom } from './utils';

interface IJsonBlogs {
    path: string;
    fields: string[][];
}
interface IImagesPaths {
    pathFiles: string;
    pathUrls: string;
}
interface IOtherRoutes {
    pathFolder: string;
    root: string;
}
export interface IOptionsCms {
    engine: 'hbs' | 'ejs' | 'njk';
    relativeTo: string;
    baseUrl: string;
    routesPath: string;
    globalsPath: string;
    customController: { [key: string]: THapiCustom };
    assetsPath?: string;
    jsonBlogs?: IJsonBlogs;
    otherRoutes?: IOtherRoutes[];
    images: IImagesPaths;
}
export const MonkeyCmsServer = {
    name: 'monkey-cms-plugin',
    version: '1.8.0',
    register: async (server: any, options: IOptionsCms) => {
        const nSite = Site(
            options.relativeTo,
            {
                pathGlobals: options.globalsPath,
                pathRoutes: options.routesPath,
            },
            options.otherRoutes,
        );
        const images = readImages(options.relativeTo);
        server.decorate('request', 'cms', nSite);
        server.decorate('request', 'cmsImages', () =>
            images(options.images.pathFiles, options.images.pathUrls),
        );

        const customControllers = options.customController
            ? options.customController
            : {};

        const config = {
            pre: PreBuildPage(nSite, images, {
                files: options.images.pathFiles,
                urls: options.images.pathUrls,
            }),
            handler: BuildPage(
                options.engine,
                options.baseUrl,
                customControllers,
            ),
        };
        server.route([
            {
                method: 'GET',
                path: '/',
                config,
            },
            {
                method: 'GET',
                path: '/{name}',
                config,
            },
            {
                method: 'GET',
                path: '/{name*2}',
                config,
            },
            {
                method: 'GET',
                path: '/{name*3}',
                config,
            },
            {
                method: 'GET',
                path: '/{name*4}',
                config,
            },
            {
                method: 'GET',
                path: '/sitemap-static',
                handler: (req: Request, h: ResponseToolkit) => {
                    return nSite.context().LINKS;
                },
            },
            {
                method: 'GET',
                path: '/sitemap-full',
                handler: (req: Request, h: ResponseToolkit) => {
                    return nSite.context().PAGES;
                },
            },
            {
                method: 'GET',
                path: options.assetsPath
                    ? `${options.assetsPath}/{param*}`
                    : `/assets/{param*}`,
                handler: {
                    directory: {
                        path: 'assets',
                    },
                },
            },
        ]);
    },
};
