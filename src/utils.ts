import { Request, ResponseToolkit } from '@hapi/hapi';

import { ISite } from 'cms-monkey-lib';

export type THapiCustom = (req: Request, h: ResponseToolkit) => any;
const Console = console;

export const BuildPage = (
    engine: string,
    baseUrl: string,
    AllCustom: { [key: string]: THapiCustom },
) => async (req: Request, h: ResponseToolkit) => {
    if (!req.pre.page) {
        return h.redirect('/404.html');
    }
    const global = { ...req.pre.page.global, IMAGES: req.pre.page.IMAGES };
    const local = req.pre.page.local;

    // const page = { global, local, BASE_URL: baseUrl, PROD: req.query.prod };

    if (global.PAGE) {
        const options: any = {};
        let context: any = {};
        const NODE_ENV = process.env.NODE_ENV;
        const STATIC = req.query.static;

        const HASH_BUNDLE = req.query.bundle || '';
        const page =
            engine === 'njk'
                ? 'templates/' + global.PAGE.view
                : global.PAGE.view;

        if (global.PAGE.layout && engine !== 'njk') {
            options.layout = global.PAGE.layout;
        }

        if (engine === 'hbs' || engine === 'ejs') {
            context = {
                ...local,
                ...global,
                BASE_URL: baseUrl,
                NODE_ENV,
                BUNDLE: global.BUNDLE
                    ? `${global.BUNDLE}${HASH_BUNDLE}`
                    : `default${HASH_BUNDLE}`,
                STATIC,
            };
        }

        if (engine === 'njk') {
            context = {
                local,
                global: {
                    ...global,
                    BASE_URL: baseUrl,
                    NODE_ENV,
                    BUNDLE: global.BUNDLE
                        ? `${global.BUNDLE}${HASH_BUNDLE}`
                        : `default${HASH_BUNDLE}`,
                    STATIC,
                },
            };
        }
        if (global.PAGE.custom) {
            if (AllCustom[global.PAGE.custom]) {
                const method = AllCustom[global.PAGE.custom](req, h);
                return await method({ context, options, page });
            } else {
                return h.redirect('/404.html');
            }
        }

        return h.view(page, context, options);
    } else {
        return h.redirect('/404.html');
    }
};

type ReadImages = (pathFiles: string, pathUrl: string) => any;
export const PreBuildPage = (
    Pages: ISite,
    IMAGES: ReadImages,
    paths: {
        files: string;
        urls: string;
    },
) => {
    return [
        [
            {
                method: (request: Request, h: ResponseToolkit) => {
                    try {
                        if (request.url.pathname === '/') {
                            return Pages.page('index.html');
                        } else {
                            const p = Pages.page(request.params.name);
                            if (p) {
                                return p;
                            } else {
                                return {};
                            }
                        }
                    } catch (error) {
                        Console.error('Error', error);
                        return {};
                    }
                },
                assign: 'context',
            },
            {
                method: (request: Request, h: ResponseToolkit) => {
                    try {
                        return IMAGES(paths.files, paths.urls);
                    } catch (error) {
                        return {};
                    }
                },
                assign: 'images',
            },
        ],
        {
            method: (request: Request, h: ResponseToolkit) => {
                return { IMAGES: request.pre.images, ...request.pre.context };
            },
            assign: 'page',
        },
    ];
};
