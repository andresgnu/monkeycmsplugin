import { THapiCustom } from './utils';
interface IJsonBlogs {
    path: string;
    fields: string[][];
}
interface IImagesPaths {
    pathFiles: string;
    pathUrls: string;
}
interface IOtherRoutes {
    pathFolder: string;
    root: string;
}
export interface IOptionsCms {
    engine: 'hbs' | 'ejs' | 'njk';
    relativeTo: string;
    baseUrl: string;
    routesPath: string;
    globalsPath: string;
    customController: {
        [key: string]: THapiCustom;
    };
    assetsPath?: string;
    jsonBlogs?: IJsonBlogs;
    otherRoutes?: IOtherRoutes[];
    images: IImagesPaths;
}
export declare const MonkeyCmsServer: {
    name: string;
    version: string;
    register: (server: any, options: IOptionsCms) => Promise<void>;
};
export {};
