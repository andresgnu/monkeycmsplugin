"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cms_monkey_lib_1 = require("cms-monkey-lib");
const utils_1 = require("./utils");
exports.MonkeyCmsServer = {
    name: 'monkey-cms-plugin',
    version: '1.8.0',
    register: (server, options) => __awaiter(this, void 0, void 0, function* () {
        const nSite = cms_monkey_lib_1.Site(options.relativeTo, {
            pathGlobals: options.globalsPath,
            pathRoutes: options.routesPath,
        }, options.otherRoutes);
        const images = cms_monkey_lib_1.readImages(options.relativeTo);
        server.decorate('request', 'cms', nSite);
        server.decorate('request', 'cmsImages', () => images(options.images.pathFiles, options.images.pathUrls));
        const customControllers = options.customController
            ? options.customController
            : {};
        const config = {
            pre: utils_1.PreBuildPage(nSite, images, {
                files: options.images.pathFiles,
                urls: options.images.pathUrls,
            }),
            handler: utils_1.BuildPage(options.engine, options.baseUrl, customControllers),
        };
        server.route([
            {
                method: 'GET',
                path: '/',
                config,
            },
            {
                method: 'GET',
                path: '/{name}',
                config,
            },
            {
                method: 'GET',
                path: '/{name*2}',
                config,
            },
            {
                method: 'GET',
                path: '/{name*3}',
                config,
            },
            {
                method: 'GET',
                path: '/{name*4}',
                config,
            },
            {
                method: 'GET',
                path: '/sitemap-static',
                handler: (req, h) => {
                    return nSite.context().LINKS;
                },
            },
            {
                method: 'GET',
                path: '/sitemap-full',
                handler: (req, h) => {
                    return nSite.context().PAGES;
                },
            },
            {
                method: 'GET',
                path: options.assetsPath
                    ? `${options.assetsPath}/{param*}`
                    : `/assets/{param*}`,
                handler: {
                    directory: {
                        path: 'assets',
                    },
                },
            },
        ]);
    }),
};
//# sourceMappingURL=index.js.map