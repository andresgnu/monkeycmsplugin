"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Console = console;
exports.BuildPage = (engine, baseUrl, AllCustom) => (req, h) => __awaiter(this, void 0, void 0, function* () {
    if (!req.pre.page) {
        return h.redirect('/404.html');
    }
    const global = Object.assign({}, req.pre.page.global, { IMAGES: req.pre.page.IMAGES });
    const local = req.pre.page.local;
    // const page = { global, local, BASE_URL: baseUrl, PROD: req.query.prod };
    if (global.PAGE) {
        const options = {};
        let context = {};
        const NODE_ENV = process.env.NODE_ENV;
        const STATIC = req.query.static;
        const HASH_BUNDLE = req.query.bundle || '';
        const page = engine === 'njk'
            ? 'templates/' + global.PAGE.view
            : global.PAGE.view;
        if (global.PAGE.layout && engine !== 'njk') {
            options.layout = global.PAGE.layout;
        }
        if (engine === 'hbs' || engine === 'ejs') {
            context = Object.assign({}, local, global, { BASE_URL: baseUrl, NODE_ENV, BUNDLE: global.BUNDLE
                    ? `${global.BUNDLE}${HASH_BUNDLE}`
                    : `default${HASH_BUNDLE}`, STATIC });
        }
        if (engine === 'njk') {
            context = {
                local,
                global: Object.assign({}, global, { BASE_URL: baseUrl, NODE_ENV, BUNDLE: global.BUNDLE
                        ? `${global.BUNDLE}${HASH_BUNDLE}`
                        : `default${HASH_BUNDLE}`, STATIC }),
            };
        }
        if (global.PAGE.custom) {
            if (AllCustom[global.PAGE.custom]) {
                const method = AllCustom[global.PAGE.custom](req, h);
                return yield method({ context, options, page });
            }
            else {
                return h.redirect('/404.html');
            }
        }
        return h.view(page, context, options);
    }
    else {
        return h.redirect('/404.html');
    }
});
exports.PreBuildPage = (Pages, IMAGES, paths) => {
    return [
        [
            {
                method: (request, h) => {
                    try {
                        if (request.url.pathname === '/') {
                            return Pages.page('index.html');
                        }
                        else {
                            const p = Pages.page(request.params.name);
                            if (p) {
                                return p;
                            }
                            else {
                                return {};
                            }
                        }
                    }
                    catch (error) {
                        Console.error('Error', error);
                        return {};
                    }
                },
                assign: 'context',
            },
            {
                method: (request, h) => {
                    try {
                        return IMAGES(paths.files, paths.urls);
                    }
                    catch (error) {
                        return {};
                    }
                },
                assign: 'images',
            },
        ],
        {
            method: (request, h) => {
                return Object.assign({ IMAGES: request.pre.images }, request.pre.context);
            },
            assign: 'page',
        },
    ];
};
//# sourceMappingURL=utils.js.map