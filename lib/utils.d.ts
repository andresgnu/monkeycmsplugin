import { Request, ResponseToolkit } from '@hapi/hapi';
import { ISite } from 'cms-monkey-lib';
export declare type THapiCustom = (req: Request, h: ResponseToolkit) => any;
export declare const BuildPage: (engine: string, baseUrl: string, AllCustom: {
    [key: string]: THapiCustom;
}) => (req: Request, h: ResponseToolkit) => Promise<any>;
declare type ReadImages = (pathFiles: string, pathUrl: string) => any;
export declare const PreBuildPage: (Pages: ISite, IMAGES: ReadImages, paths: {
    files: string;
    urls: string;
}) => ({
    method: (request: Request, h: ResponseToolkit) => any;
    assign: string;
}[] | {
    method: (request: Request, h: ResponseToolkit) => any;
    assign: string;
})[];
export {};
